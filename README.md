# About webcrawler

A really simple means of finding all the links in a website

Add a config file


```
#!javascript

{
	//checking for start of hrefs
	"allowedLinksRegExs": [
		"https:\\/\\/m.example.com",
        "\\/(?!secure|go\\/|lp\\/|example\\?)[A-Za-z]+"	
	],
	//level refers to numbe of slashes in url e.g. /something/other = 2  
    "levelToLoad": 1
}

```



#### TODO

1. Promisify everything
2. Write some tests