'use strict';

const fetchUrl = require("fetch").fetchUrl;
const now = require('performance-now')
const R = require("ramda");
const cheerio = require('cheerio');
const Promise = require("bluebird");
const fs = Promise.promisifyAll(require("fs"));
const url = require('url');

const webcrawl = (config) => {  

    const hrefCheck = link => link.attribs && link.attribs.href;
    const allowedLinksRegExsPattern = new RegExp("^(" + config.allowedLinksRegExs.join('|') + ")","gmi")
    const stringTest = link => R.test(allowedLinksRegExsPattern, link.attribs.href);
    const filterLinks = R.compose(R.filter(stringTest), R.filter(hrefCheck));
    const pluckLink = R.compose(R.map(R.trim), R.pluck('href'), R.pluck('attribs'))

    const uniq = R.uniq();
    const diff = (a,b) => a.localeCompare(b);
    const sort = R.sortWith([
        R.ascend(R.prop('level')),
        R.ascend(R.prop('name'))
    ]);

    const removeQuery = R.compose(R.head, R.split('?'));
    const removeAnchor = R.compose(R.head, R.split('#'));
    const stripLink = R.map(R.compose(R.replace(domainUrl, ''), removeAnchor, removeQuery ));
    const createString = str => {
        const level = (str.match(new RegExp("/", "g")) || []).length;
        return {
            name: R.replace(/(\/|\-|\%28|\%29)/g,'',str),
            link: str,
            linkedAt: linkUrl,
            level: level,
            toLoad: level <= config.levelToLoad
        };
    }
    const createObjects = R.map(createString);
    const mergeObjects = (obj) => {
        return R.unionWith(R.eqBy(R.prop('name')), linkStore, obj)
    }
    const log = R.tap(console.log)    

    const createLinkObjects = R.pipe(
        filterLinks,
        pluckLink,
        stripLink,
        uniq,
        createObjects,
        mergeObjects,
        sort
    )

    const jsonWriteHandler = (err) => {
        if (err) console.log('Write failed', err);
        console.log('Links written to file');
    }

    const curateLinks = (err, response, body) => {
        if (err) console.log('there was a problem');
        linkStore[pageLoading].responseCode = response.code
        let $ = cheerio.load(body);
        let links = [];
        $("a").each((i,link) => links.push(link));
        linkStore = createLinkObjects(links);

        pageLoading = getNextPageToLoad();
        if(pageLoading) {
            linkStore[pageLoading].toLoad = false;
            linkUrl = domainUrl + linkStore[pageLoading].link
            console.log('Getting: ', linkUrl);
            setTimeout(() => {
                getPageDocument();
            },1000)
        } else {
            fs.writeFileAsync(config.writeFile, JSON.stringify(linkStore), 'utf8')
                .then(() => console.log('everything good'))
                .catch(() => console.log('something went wrong'));
        }
    
    }
    
    const getPageDocument = () => fetchUrl(linkUrl, curateLinks)

    const getNextPageToLoad = () => {  
        for(var i = 0; i < linkStore.length; i++) {
            if(linkStore[i].toLoad == true) {
                return i;
            }
        }
        return false;
    }

    getPageDocument();

}


/**----------------------------**\ 
        initial set up 
\**----------------------------**/
let linkUrl =  process.argv[2]
if (!linkUrl) return console.error('Please add a URL')

const baseUrlObject = url.parse(linkUrl)
const domainUrl = baseUrlObject.protocol + '//' + baseUrlObject.hostname;

let linkStore = [{
    'name': 'start',   
    'link': linkUrl,
    'level': (linkUrl.split(domainUrl)[1].match(new RegExp("/", "g")) || []).length,
    'toLoad': false   
}];
let pageLoading = 0;
 
fs.readFileAsync('./config.json', 'utf8')
  .then(data => webcrawl(JSON.parse(data)))
  .catch(e => console.error(e))
